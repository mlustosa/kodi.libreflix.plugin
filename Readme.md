# Plugin Libreflix para Kodi
O objetivo da criação deste plugin foi o de ampliar a cobertura de clientes de software da plataforma [Libreflix](https://www.libreflix.org).
Perfeito para rodar em dispositivos arm (como Raspberry Pi), rodando [OSMC](https://osmc.tv/) ou [LibreElec](https://libreelec.tv/).

Este plugin foi testado em Kodi instalado em GNU/Linux (x86_64) e em OSMC (RPi/arm).

Baseado no [Simple example plugin](https://github.com/romanvm/plugin.video.example)
License: [GPL v.3](http://www.gnu.org/copyleft/gpl.html)


# Instruções de instalação

Faça download do projeto (ZIP) e aplique o zip pela interface do Kodi em My Addons>Install zip repository.

OBS: Plugin ainda não utilizável em produção, porém funcional em apresentação.
Necessitando da adição das categorias e links.